from libqtile import bar
from .widgets import *
from libqtile.config import Screen
import os

screens = [
    Screen(
        top=bar.Bar(
            [widget.Sep(padding=3, linewidth=0, background="#2f343f"),
                widget.TextBox(text='ʵ', font="icomoon", fontsize="25", background="#2f343f", mouse_callbacks={
                               'Button1': lambda: qtile.cmd_spawn("rofi -show combi")}),
                widget.Sep(padding=4, linewidth=0, background="#2f343f"),
                widget.GroupBox(
                highlight_method='line',
                this_screen_border="#5294e2",
                this_current_screen_border="#ff0090",
                active="#ffffff",
                inactive="#848e96",
                background="#2f343f"),
                widget.TextBox(
                text='◤',
                padding=-3,
                fontsize=65,
                foreground='#2f343f'
            ),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget.WindowName(foreground='#99c0de',
                                  fmt='{}', max_chars=30),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
            ),
                widget.Image(
                    filename="~/.config/qtile/powerline_3.png"
            ),
                widget.CurrentLayoutIcon(scale=0.75, background='#ff0090'),
                widget.Image(
                    filename="~/.config/qtile/powerline_2.png"
            ),
                widget.Net(
                    background='#16181d',
                    foreground='#ffffff',
                    format='↓↑ {down}',
                    max_chars=10
            ),
                widget.Image(
                    filename="~/.config/qtile/powerline_1.png"
            ),
            widget.TextBox(
                text="",
                background="#ff0090",
                foreground="#ffffff",
                font="icomoon",
                fontsize=20
            ),
                widget.Memory(
                format='{MemUsed: .0f}{mm}/{MemTotal: .0f}{mm}',
                background='#ff0090',
                foreground='#ffffff',
            ),
                widget.Image(
                    filename="~/.config/qtile/powerline_2.png"
            ),
                widget.Clock(format=' %a %d %b, %I:%M ',
                             background="#16181d",
                             foreground='#ffffff'),
                widget.Image(
                    filename="~/.config/qtile/powerline_1.png"
            ),
                widget.Systray(icon_size=20, background='#ff0090'),
                widget.TextBox(
                    text='',
                    mouse_callbacks={
                        'Button1':
                        lambda: qtile.cmd_spawn(os.path.expanduser(
                            '~/.config/rofi/powermenu.sh'))
                    },
                    foreground='#ffffff',
                    background='#ff0090',
                    padding=10
            )

            ],
            30,  # height in px
            background="#404552",  # background color
            font="JetBrains Mono"
            # margin=[4,4,0,4],
            # opacity=0.6
        ), ),
]
