from libqtile.config import Key, Group, Match
from libqtile.lazy import lazy
from .keys import keys, mod

groups = [
    Group('1', label="WWW", matches=[Match(wm_class=["firefox", "Vivaldi-stable"])], layout="max"),
    Group('2', label="DEV", matches=[Match(wm_class=["Sublime_text", "Code"])]),
    Group('3', label="SYS", matches=[Match(wm_class=["Xfce4-terminal"])]),
    Group('4', label="CHAT", matches=[Match(wm_class=["TelegramDesktop"])]),
    Group('5', label="ETC", matches=[Match(wm_class=["Thunar", "Pamac-manager", "ark"])]),
    Group('6'),
    Group('7'),
    Group('8'),
    Group('9'),
]

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "l", lazy.screen.next_group()),
        Key([mod], "j", lazy.screen.prev_group()),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True)),
    ])
